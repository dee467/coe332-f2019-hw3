FROM python:3-onbuild

LABEL maintainer = "Daniel Espinoza <danielespinoza@utexas.edu"

EXPOSE 5000
ADD main.py /
ADD app /
ADD requirements.txt /


RUN pip install -r requirements.txt

ENTRYPOINT ["python"]
CMD ["main.py"]
 
