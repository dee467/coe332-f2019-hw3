from flask import Flask, jsonify, request,abort
import json

# The main Flask app
app = Flask(__name__)

# Data from a json file
data = json.load(open('coe332.json', 'r'))
#Display all
@app.route('/')
def coe332():
    return jsonify(data)
@app.route('/meeting')
def meeting():
	meet = data['meeting']
	return jsonify(meet)

@app.route('/meeting/days')
def days():
	meet = data['meeting']['days']
	return jsonify(meet)

@app.route('/meeting/location')
def location():
	meet = data['meeting']['location']
	return jsonify(meet)

@app.route('/meeting/end')
def end():
	meet = data['meeting']['end']
	return jsonify(meet)

@app.route('/meeting/start')
def start():
	meet = data['meeting']['start']
	return jsonify(meet)
@app.route('/instructors')
## INSTRUCTORS
def instructors():
	instr = data['instructors']
	return jsonify(instr)
@app.route('/instructors/<int:number>', methods=['GET'])
def index(number):
		if number>=len(data['instructors']):
			abort(404)
		instr= data['instructors'][number]
		return jsonify(instr)
#ASSIGNMENTS

@app.route('/assignments', methods=['POST','GET'])
def assignments():
	if request.method=='POST':
		entry= request.get_json()
		data['assignments'].append(entry)
		return jsonify (data['assignments'])
	else:
		return jsonify(data['assignments'])
#Index
@app.route('/assignments/<int:number>', methods=['GET'])
def hwnum(number):
	if number>=len(data['assignments']):
			abort(404)
	return jsonify(data['assignments'][number])

#Points
@app.route('/assignments/<int:number>/points', methods=['GET'])
def points(number):
		return jsonify(data['assignments'][number]['points'])
#url
@app.route('/assignments/<int:number>/url', methods=['GET'])
def url(number):
	return jsonify(data['assignments'][number]['url'])



